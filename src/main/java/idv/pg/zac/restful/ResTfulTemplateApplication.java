package idv.pg.zac.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResTfulTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResTfulTemplateApplication.class, args);
	}

}
