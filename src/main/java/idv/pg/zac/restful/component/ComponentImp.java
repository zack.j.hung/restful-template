package idv.pg.zac.restful.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import idv.pg.zac.restful.service.ComType;
import idv.pg.zac.restful.service.ComponentService;

@Service("componentImp") 
public class ComponentImp implements ComponentService {

	private ComType type = ComType.UNKNOW;
	
	@Autowired
	private Dll comDLL;
	
	@Autowired
	private JAR comJAR;
	
	@Override
	public String getVer() {
		String ver = "";
		
		switch (type) {
		case JAR:
			ver = comJAR.getVer();
			break;
			
		case DLL:
			ver = comDLL.getVer();
			break;
			
		default :
			ver = "unknow";
		}
		
		return ver;
	}

	@Override
	public void setCom(String s) {
		type = ComType.UNKNOW;
		for (ComType t : ComType.values()) { 
		    if (s.equalsIgnoreCase(t.getType())) {
		    	type = t;
		    	break;
		    }
		}
	}


}
