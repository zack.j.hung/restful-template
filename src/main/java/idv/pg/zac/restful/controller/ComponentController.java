package idv.pg.zac.restful.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import idv.pg.zac.restful.service.ComponentService;

@RestController
@RequestMapping("/component")
public class ComponentController {

	@Autowired
	@Qualifier("componentImp")
	private ComponentService component;
	
	@GetMapping(value = "/info", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
	public String componentInfo() {
		
		return "not support!";
	}
	
	@GetMapping(value = "/info/{comname}", consumes = MediaType.ALL_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
	public String componentInfo(@PathVariable("comname") String comName) {
		// comname = <jar | dll>
		component.setCom(comName);
		return component.getVer();
	}
}
