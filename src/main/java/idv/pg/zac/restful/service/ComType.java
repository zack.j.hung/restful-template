package idv.pg.zac.restful.service;

public enum ComType {
	JAR("jar"), DLL("dll"), UNKNOW("0.0.0-unknow");
	
	private String com;
	
	ComType(String s) {
		com = s;
	}
	
	public String getType() {
		return com;
	}
}
